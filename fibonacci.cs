using System;
using System.Threading.Tasks;
using System.Threading;


namespace ElektaTest
{
    public class ProgramMain
    {
 		public void Main(string[] args)
		{
			Thread thr = System.Threading.Thread.CurrentThread;
			Console.WriteLine("Main thread id: {0}", Thread.CurrentThread.ManagedThreadId);
			Console.WriteLine("*******************\n");
			bool Valid = false; int Number;	string input = "0";

			Console.Write("Enter a number - ");

			// Validate the input number. Number should be integer and greater than 0
			while(Valid == false)
			{
			  	input = Console.ReadLine(); 
			  	if(int.TryParse(input, out Number) && Convert.ToInt32(input) > 0)
				{ 
					Valid = true; 
			  	} 
			  	else
				{ 
					Console.Write("Please enter a valid length again!");
			  	} 
			} 			

			
			//Wait till main thread completes the asynchronous Fibonacci operation
			Task.Run(async () =>
			{
				await Fibonacci.Calculate_Fibonacci(Convert.ToInt32(input));
			}).GetAwaiter().GetResult();			

		}
	}
	
	public class Fibonacci
	{		
		/// <summary>
		/// Main function calls the recurssive fibonacci function
		/// </summary>
		/// <param name="len">Limit untill Fibonacci series needs to be calculated
		public static async Task Calculate_Fibonacci(int len)  
		{  
			Console.WriteLine("\n\nCalculate_Fibonacci method is running in thread id: {0}", Thread.CurrentThread.ManagedThreadId);
		   	await Fibonacci.Fibonacci_Reccursive(0, 1, 1, len);
		}

		/// <summary>
		/// Recurssive function calculates the Fibonacci series recursively
		/// in a separate thread
		/// </summary>
		/// <param name="a">Previous number
		/// <param name="b">Next number
		/// <param name="counter">Current execution counter
		/// <param name="len">Limit untill Fibonacci series needs to be calculated
		private static async Task Fibonacci_Reccursive(int a, int b, int counter, int len)  
		{   
			if (counter <= len)  
			{  
				Console.WriteLine("*************************************************\n\n");
				Console.WriteLine("Current Fibonnaci value is {0} ", a);
				
				// created two async function which caculates the values and wait for 500 milliseconds
				int result = await Task.Run(() => Calculate(a, b));
				int current_counter = await Task.Run(() => Fibonacci.Increase_Counter(counter));
				
				// Recursively calling 
			    await Fibonacci_Reccursive(b, result, counter+1, len);  
			}
			else
			{
				Console.WriteLine("\n#####################################\n");
				Console.Write("Total Fibonnaci count is - {0}", b-1);
			}
		}
		
		
		/// <summary>
		/// async function which just adds 550 milliseconds delay and returns the addition of two numbers
		/// </summary>
		/// <param name="a">Previous number
		/// <param name="b">Next number
		private static async Task<int> Calculate(int x, int y)
	    {
			Console.WriteLine("Calculated method is running in thread id: {0}", Thread.CurrentThread.ManagedThreadId);			
			await Task.Delay(500);
			return x + y;
    	}
		
		/// <summary>
		/// async function which just adds 550 milliseconds delay and increases the counter
		/// </summary>
		/// <param name="a">Previous number
		/// <param name="b">Next number
		private static async Task<int> Increase_Counter(int counter)
	    {
			Console.WriteLine("Increase_counter method is running in thread id: {0}", Thread.CurrentThread.ManagedThreadId);						
			await Task.Delay(500);
			return counter + 1;
    	}				
	}
}